# Commands

## Start up a machine
```bash
    vagrant up
```

## SSH into a machine
```bash
    vagrant ssh
```

## Leave SSH session
```bash
    logout
```

## Suspend a machine
```bash
    vagrant suspend
```

## Resume a suspended machine
```bash
    vagrant resume
```

## Restart a machine
```bash
    vagrant reload
```
This action can be performed after adding configs to the Vagrantfile in order to reflect those changes on the new start up of the machine. 
## Shut down a machine (Power off)
```bash
    vagrant halt
```
## Destroy a Vagrant VM

```bash
    vagrant destroy
```