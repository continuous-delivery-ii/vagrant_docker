# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # Box Settings (a box is a operating system)
  config.vm.box = "generic/ubuntu2004"
  
  # SSH Settings
  # config.ssh.username = 'vagrant'
  # config.ssh.password = 'vagrant'
  # config.ssh.insert_key = "false"

  # Provider Settings
  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    # vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "2048"
    # Customize the amount of CPUs on the VM:
    vb.cpus = 4

    # file_to_disk = './tmp/second_disk.vdi'
    disk = './tmp/second_disk.vdi'
  
    unless File.exist?(disk)
      vb.customize [ "createmedium", "disk", "--filename", disk, "--format", "vdi", "--size", 1024 * 30 ]
    end

    vb.customize ['storageattach', :id, '--storagectl', 'IDE Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', File.absolute_path(disk)]

    vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
  end

  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Networks Settings
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.100.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network", ip: "192.168.100.1"

  # Folder Settings
  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provision Settings

  # Mount second disk
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbooks/mount-partition-playbook.yml"
  end
  # Install Docker
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbooks/docker-playbook.yml"
  end
  # Change Docker data directory
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbooks/change-docker-location.yml"
  end
  # Install k3d
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbooks/install-k3d.yml"
  end
end
